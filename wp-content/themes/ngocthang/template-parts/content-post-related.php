<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ngocthang
 */

?>

<div class="care-item">
    <a href="<?php the_permalink(); ?>">
        <div class="care-img">
        <?php the_post_thumbnail('full', array( 'class' => 'img-fluid center-block' ) ); ?>
        </div>
        <div class="care-text">
            <div class="title"><?php the_title(); ?></div>
            <div class="desc"><?php echo ngocthang_limit_words(get_the_excerpt(),30); ?></div>
        </div>
        <div class="care-meta-data"><?php the_time('d/m/Y'); ?></div>
    </a>
</div>