<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ngocthang
 */

?>

<div class="content-news-box">
	<a href="<?php the_permalink(); ?>">
		<div class="content-news-img">
		<?php the_post_thumbnail('full', array( 'class' => 'img-fluid center-block' ) ); ?>
		</div>
	</a>
	<div class="content-news-title">
		<div class="content-news-title-item">
		<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		</div>
		<div class="content-news-date">
			<div class="content-news-date-item"><?php the_time('d/m/Y'); ?></div>
		</div>
		<div class="content-news-text">
			<p><?php echo ngocthang_limit_words(get_the_excerpt(),30); ?></p>
		</div>
		<a href="<?php the_permalink(); ?>">
			<div class="content-news-btn">Đọc tiếp</div>
		</a>
	</div>
</div>