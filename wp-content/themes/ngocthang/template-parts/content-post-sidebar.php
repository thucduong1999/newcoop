<?php

/**

 * Template part for displaying posts

 *

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/

 *

 * @package ngocthang

 */



?>

<div class="outstanding-box-list">
	<div class="outstanding-img news-outstanding-img">
		<a href="<?php the_permalink(); ?>">
			<?php the_post_thumbnail('full', array( 'class' => 'img-fluid center-block' ) ); ?>
		</a>
	</div>
	<div class="outstanding-title">
		<div class="outstanding-title-item">
			<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
		</div>
		<div class="outstanding-date">
			<div class="outstanding-date-item"><?php the_time('d/m/Y'); ?></div>
		</div>
		<div class="outstanding-text">
			<p><?php echo ngocthang_limit_words(get_the_excerpt(),30); ?>.</p>
		</div>
		<a href="<?php the_permalink(); ?>">
			<div class="outstanding-btn">Đọc tiếp</div>
		</a>
	</div>
</div>
<!-- #post-<?php the_ID(); ?> -->

