<?php
/*
Template Name:Page Cua hang
Template Post Type: post, page, event
*/
get_header();
?>
<div class="page-cua-hang">
<section class="header header-store">
	<div class="header-box store-background">
		<div class="header-title">
			<h1><?php the_title(); ?></h1>
		</div>
	</div>
</section>

<section class="map">
	<div class="box">
		<div class="store-bottom">
			<div class="store-bottom-left">
				<div class="list">
					<div class="wrap">
						<?php
						$args = array(
							'post_type' => 'post',
							'category_name' => 'chi-nhanh',
							'orderby' => 'date',
							'order' => 'DESC',
							'posts_per_page' => -1,
						);
						$the_query = new WP_Query( $args );
						// The Loop
						if ( $the_query->have_posts() ) :
							while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
								<a href="<?php the_permalink(); ?>">
									<div class="list-item">
										<h1><?php the_title(); ?></h1>
										<p><?php echo  $value = rwmb_meta( 'address-store' );?></p>
										<div class="phone"><?php echo  $value = rwmb_meta( 'phone-store' );?></div>
									</div>
								</a>
							<?php endwhile;
						endif;
						// Reset Post Data
						wp_reset_postdata();
						?>
					</div>
				</div>
			</div>
			<div class="store-bottom-right">
				<div class="wrap">
					<div class="test" style="background-color:aqua ; width: 100%; height: 700px;">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.674928002218!2d105.85264231476285!3d21.00566388601089!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac0a8d3eabc5%3A0xbc5d6e1f03c8f80f!2zODAgVsO1IFRo4buLIFPDoXUsIFRoYW5oIE5ow6BuLCBIYWkgQsOgIFRyxrBuZywgSMOgIE7hu5lpLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1653290672325!5m2!1svi!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="coop">
	<div class="box">
		<div class="wrap">
			<div class="content-post">
				<?php the_content(); ?>
			</div>
			<div class="social">
				<span class="social-box">
					<a href="#"><i class="fab fa-facebook icon"></i></a>
				</span>
				<span class="social-box">
					<a href="#"><i class="fab fa-instagram icon"></i></a>
				</span>
			</div>
		</div>
	</div>
</section>
</div>
	<?php
	get_footer();