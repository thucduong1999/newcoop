<?php
/*
Template Name:Page san pham
Template Post Type: post, page, event
*/
get_header();
?>
<div class="breadcrumbs">
    <div class="header header-san-pham">
        <div class="header-title">
            <h1><?php the_title(); ?></h1>
        </div>
    </div>
</div>

<div class="milk-img-box">
    <div class="categoty-milk">
        <?php for($i=1;$i<=6;$i++) { ?>
                <div class="milk-box-list">
                    <div class="wrap-5">
                        <a href="<?php echo of_get_option('link-cate'.$i); ?>">
                            <div class="milk-image-box">
                                <div class="milk-img-list">
                                    <div class="milk-img-item">
                                    <img src="<?php echo of_get_option('image-cate'.$i); ?>">
                                    </div>
                                    <div class="milk-img-text"><?php echo of_get_option('name-cate'.$i); ?></div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            <?php }
            ?>
    </div>
</div>
<section class="milks">
    <div class="box">
        <section class="fresh-milk">
            <div class="fresh-milk-title">
                <h2>GoA Milk</h2>
            </div>
            <div class="wrap">
                <div class="gird">
                    <div class="row">
                    <?php
					$project_post_args = array(                 
						'post_type' => 'san-pham',
                        
						'posts_per_page' => '3',
					); ?>
					<?php $wp_query = new WP_Query( $project_post_args );
					if ( $wp_query -> have_posts() ) : 
						while ( $wp_query -> have_posts() ) : $wp_query -> the_post(); ?>
                            <div class="row-item">
                                <div class="row-box-img">
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="row-item-img">
                                        <?php the_post_thumbnail('full', array( 'class' => 'img-fluid center-block' ) ); ?>
                                            <div class="row-background"></div>
                                            <div class="product-row-details">
                                                <div class="details">
                                                    Chi tiết
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="row-item-text">
                                    <a href="<?php the_permalink(); ?>">
                                        <span><?php the_title(); ?></span>
                                    </a>
                                </div>
                            </div>
                        <?php endwhile; endif; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <section class="milk-set">
        <div class="box">
            <div class="milk-set-box">
                <div class="milk-set-title">
                    <h2>GoA Yogurt</h2>
                </div>
                <div class="wrap">
                <div class="gird">
                    <div class="row">
                    <?php
					$project_post_args = array(                 
						'post_type' => 'san-pham',
                        
						'posts_per_page' => '3',
					); ?>
					<?php $wp_query = new WP_Query( $project_post_args );
					if ( $wp_query -> have_posts() ) : 
						while ( $wp_query -> have_posts() ) : $wp_query -> the_post(); ?>
                            <div class="row-item">
                                <div class="row-box-img">
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="row-item-img">
                                        <?php the_post_thumbnail('full', array( 'class' => 'img-fluid center-block' ) ); ?>
                                            <div class="row-background"></div>
                                            <div class="product-row-details">
                                                <div class="details">
                                                    Chi tiết
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="row-item-text">
                                    <a href="<?php the_permalink(); ?>">
                                        <span><?php the_title(); ?></span>
                                    </a>
                                </div>
                            </div>
                        <?php endwhile; endif; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </section>

    <section class="milk-st">
        <div class="box">
            <div class="fresh-milk">
                <div class="fresh-milk-title">
                    <h2>GoA Rice</h2>
                </div>
                <div class="wrap">
                <div class="gird">
                    <div class="row">
                    <?php
					$project_post_args = array(                 
						'post_type' => 'san-pham',
                        
						'posts_per_page' => '3',
					); ?>
					<?php $wp_query = new WP_Query( $project_post_args );
					if ( $wp_query -> have_posts() ) : 
						while ( $wp_query -> have_posts() ) : $wp_query -> the_post(); ?>
                            <div class="row-item">
                                <div class="row-box-img">
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="row-item-img">
                                        <?php the_post_thumbnail('full', array( 'class' => 'img-fluid center-block' ) ); ?>
                                            <div class="row-background"></div>
                                            <div class="product-row-details">
                                                <div class="details">
                                                    Chi tiết
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="row-item-text">
                                    <a href="<?php the_permalink(); ?>">
                                        <span><?php the_title(); ?></span>
                                    </a>
                                </div>
                            </div>
                        <?php endwhile; endif; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </section>

    <div class="yogurt">
        <div class="box">
            <div class="milk-set-box">
                <div class="milk-set-title">
                    <h2>Dịch vụ nông nghiệp New Coop</h2>
                </div>
                <div class="wrap">
                <div class="gird">
                    <div class="row">
                    <?php
					$project_post_args = array(                 
						'post_type' => 'san-pham',
                        
						'posts_per_page' => '3',
					); ?>
					<?php $wp_query = new WP_Query( $project_post_args );
					if ( $wp_query -> have_posts() ) : 
						while ( $wp_query -> have_posts() ) : $wp_query -> the_post(); ?>
                            <div class="row-item">
                                <div class="row-box-img">
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="row-item-img">
                                        <?php the_post_thumbnail('full', array( 'class' => 'img-fluid center-block' ) ); ?>
                                            <div class="row-background"></div>
                                            <div class="product-row-details">
                                                <div class="details">
                                                    Chi tiết
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="row-item-text">
                                    <a href="<?php the_permalink(); ?>">
                                        <span><?php the_title(); ?></span>
                                    </a>
                                </div>
                            </div>
                        <?php endwhile; endif; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>


<?php
	get_footer();