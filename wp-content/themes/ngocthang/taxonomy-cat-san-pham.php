<?php
/**
 * The template for displaying Archive pages
 *
 * @package    WordPress
 * @subpackage KenIT
 * @author     Quyền Nguyễn
 * @version    1.0
 * @link       www.kenit.net
 */
get_header(); ?>
<div class="breadcrumbs">
    <div class="header-list-product header-san-pham">
        <div class="header-title">
            <?php
            the_archive_title( '<h1 class="h1-title">', '</h1>' );
            ?>
        </div>
    </div>
</div>
<section class="archive__milks">
        <div class="milks container">
          <div class="gird">
            <div class="row">
            <?php if ( have_posts() ) : ?>
                        <?php
                        /* Start the Loop */
                        while ( have_posts() ) :
                          the_post();?>
                          <div class="row-item">
                              <div class="row-box-img">
                                  <a href="<?php the_permalink(); ?>">
                                      <div class="row-item-img">
                                      <?php the_post_thumbnail('full', array( 'class' => 'img-fluid center-block' ) ); ?>
												               <div class="row-background"></div>
                                          <div class="product-row-details">
                                              <div class="details">
                                                  Chi tiết
                                              </div>
                                          </div>
                                      </div>
                                  </a>
                              </div>
                              <div class="row-item-text">
                                  <a href="<?php the_permalink(); ?>">
                                      <span><?php the_title(); ?></span>
                                  </a>
                              </div>
                          </div>
                        <?php endwhile;

                        the_posts_navigation(); 
                      else :
                        get_template_part( 'template-parts/content', 'none' );
                      endif;
                      ?>
            </div>
        </div>
        </div>
          
</section>
<?php get_footer();
