<?php
add_filter( 'rwmb_meta_boxes', function ( $meta_boxes ) {

    $meta_boxes[] = array(
        'title'  => 'Thông tin chi nhánh',
        'post_types' => array( 'post' ),
        'fields' => array(
            array(
                'id'   => 'address-store',
                'name' => 'Địa chỉ cửa hàng',
                'type'             => 'text',
            ),
            array(
                'id'   => 'phone-store',
                'name' => 'Hotline cửa hàng',
                'type'       => 'text',
            ),

        ),
    );


    $meta_boxes[] = array(
        'title'  => 'Thông tin sản phẩm',
        'post_types' => array( 'san-pham' ),
        'fields' => array(
            array(
                'id'   => 'info-pro',
                'name' => 'Thông số sản phẩm',
                'type'    => 'wysiwyg',
                'raw'     => false,
                'options' => [
                    'textarea_rows' => 8,
                    'teeny'         => true,
                ]
            ),
            array(
                'id'   => 'info-pro-2',
                'name' => 'Sologan quảng cáo',
                'type'    => 'wysiwyg',
                'raw'     => false,
                'options' => [
                    'textarea_rows' => 8,
                    'teeny'         => true,
                ]
            ),

        ),
    );

    return $meta_boxes;
});