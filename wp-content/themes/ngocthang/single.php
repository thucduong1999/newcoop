<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ngocthang
 */
get_header();
?>
<section class="header">
	<div class="header-box news-detail-background">
		<div class="header-title news-details-title">
			<h1 class="h1-title"><?php the_title(); ?></h1>
			<div class="news-details-desc">
				<div class="list-text">Tác giả: <?php echo get_the_author(); ?></div>
				<div class="list-text">Ngày đăng: <?php the_time('d/m/Y'); ?>2</div>
				<div class="list-text">Thư mục: Tin tức</div>
			</div>
		</div>
		
	</div>
</section>
<main class="single__content">
	<div class="container">
				<h1 class="h1-title"><?php the_title(); ?></h1>
				<?php if (rwmb_meta('diadiem')) { ?>
					<div class="info-price">
						<p class="address1"><strong>Địa điểm :</strong><?php echo  $value = rwmb_meta('diadiem'); ?></p>
						<span class="price"><strong>Giá :</strong>
							<?php echo  $value = rwmb_meta('gia'); ?>
						</span>
						<span class="numberS"><strong>Diện tích:</strong></i><?php echo  $value = rwmb_meta('dientich'); ?></span>

					</div>
				<?php } ?>

				<!-- 	<?php ngocthang_posted_on(); ?>
			<?php ngocthang_posted_by(); ?> -->
				<?php
				/* Start the Loop */
				while (have_posts()) :
					the_post(); ?>
					<article class="entry-content">
						<?php the_content(); ?>
					</article>
				<?php endwhile; // End of the loop.
				?>
				<div class="tags">
					<?php the_tags('Tags:', ',', ','); ?>
				</div>

				<?php
				// If comments are open or we have at least one comment, load up the comment template.
				if (comments_open() || get_comments_number()) {
					comments_template();
				}
				?>

				<!-- <div class="social news-details-social">
					<div class="details-box"><a href=""><i class="fab fa-facebook"></i></a></div>
					<div class="details-box"><a href=""><i class="fab fa-twitter"></i></a></div>
					<div class="details-box"><a href=""><i class="fab fa-linkedin"></i></a></div>
					<div class="details-box"><a href=""><i class="fab fa-pinterest"></i></a></div>
				</div>
				<div class="rating-cmt">
					<div class="wrap rating-border">
						<div class="rating">
							<div class="rating-left-right"></div>
							<div class="rating-data">
								<div class="rating-value">
									<span class="wpv">0</span>
									<span class="wpc">0</span>
									<span class="wpt">Các đánh giá</span>
								</div>
								<div class="rating-title">Article Rating</div>
								<div class="stars rating-stars">
									<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path d="M0 0h24v24H0z" fill="none"></path>
										<path class="wpd-star" d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z"></path>
										<path d="M0 0h24v24H0z" fill="none"></path>
									</svg>
									<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path d="M0 0h24v24H0z" fill="none"></path>
										<path class="wpd-star" d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z"></path>
										<path d="M0 0h24v24H0z" fill="none"></path>
									</svg>
									<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path d="M0 0h24v24H0z" fill="none"></path>
										<path class="wpd-star" d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z"></path>
										<path d="M0 0h24v24H0z" fill="none"></path>
									</svg>
									<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path d="M0 0h24v24H0z" fill="none"></path>
										<path class="wpd-star" d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z"></path>
										<path d="M0 0h24v24H0z" fill="none"></path>
									</svg>
									<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path d="M0 0h24v24H0z" fill="none"></path>
										<path class="wpd-star" d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z"></path>
										<path d="M0 0h24v24H0z" fill="none"></path>
									</svg>
								</div>
								<div class="stars rate-stars">
									<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path d="M0 0h24v24H0z" fill="none"></path>
										<path class="wpd-star" d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z"></path>
										<path d="M0 0h24v24H0z" fill="none"></path>
									</svg>
									<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path d="M0 0h24v24H0z" fill="none"></path>
										<path class="wpd-star" d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z"></path>
										<path d="M0 0h24v24H0z" fill="none"></path>
									</svg>
									<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path d="M0 0h24v24H0z" fill="none"></path>
										<path class="wpd-star" d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z"></path>
										<path d="M0 0h24v24H0z" fill="none"></path>
									</svg>
									<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path d="M0 0h24v24H0z" fill="none"></path>
										<path class="wpd-star" d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z"></path>
										<path d="M0 0h24v24H0z" fill="none"></path>
									</svg>
									<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path d="M0 0h24v24H0z" fill="none"></path>
										<path class="wpd-star" d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z"></path>
										<path d="M0 0h24v24H0z" fill="none"></path>
									</svg>
								</div>
							</div>
							<div class="rating-left-right"></div>
						</div>
						<div class="cmt">
							<div class="form-wrap">
								<div class="form-header">
									<div class="flex toggle" id="toggle">
										<i class="far fa-envelope"></i>
										<div class="title">Đăng ký</div>
										<i class="fas fa-caret-down"></i>
									</div>
									<div class="flex auth">
										<div class="social-login">
											Kết nối với
											<i class="fab fa-google"></i>
										</div>
										<div class="login">
											<a href="#">
												<i class="fas fa-sign-in-alt"></i>
												Đăng nhập
											</a>
										</div>
									</div>
								</div>
								<div class="subcribe">
									<form action="post">
										<div class="subc-info">Thông báo về</div>
										<div class="subc-option">
											<select name="subc_option">
												<option value="post">Theo dõi bình luận mới nhất</option>
												<option value="all_comment">Trả lời mới nhất cho nhận xét của tôi</option>
											</select>
										</div>
										<div class="subc-email">
											<input class="email" name="subc_email" type="text" placeholder="Email" required>
										</div>
										<div class="subc-button">
											<input class="button" type="submit" value=">">
										</div>
										<div class="subc-captcha">
											<div class="recaptcha">
												<div style="width: 304px; height: 78px;">
													<iframe title="reCAPTCHA" src="https://www.google.com/recaptcha/api2/anchor?ar=1&amp;k=6LccEIkcAAAAAB2ckEoPDtqFZ_uTF2yjUkhCo7fb&amp;co=aHR0cHM6Ly9ldmVyZ3Jvd3RobWlsay52bjo0NDM.&amp;hl=en&amp;v=g9jXH0OtfQet-V0Aewq23c7K&amp;theme=light&amp;size=normal&amp;cb=jtr28x42wxss" width="304" height="78" role="presentation" name="a-gvqi07f4kk5m" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox allow-storage-access-by-user-activation"></iframe>
													<textarea id="g-recaptcha-response-1" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid rgb(193, 193, 193); margin: 10px 25px; padding: 0px; resize: none; display: none;"></textarea>
												</div>
											</div>
										</div>
									</form>
								</div>
								
								<div class="form-foot">
									<div class="form-foot-left">
										<div class="form-name form-input">
											<div class="icon">
												<i class="fas fa-user"></i>
											</div>
											<input type="text" name="name" require placeholder="Name*">
										</div>
										<div class="form-email form-input">
											<div class="icon">
												<i class="fas fa-at"></i>
											</div>
											<input type="email" name="email" require placeholder="Email*">
										</div>
										<div class="form-website form-input">
											<div class="icon">
												<i class="fas fa-link"></i>
											</div>
											<input type="text" name="website" require placeholder="Website*">
										</div>
									</div>
									<div class="form-foot-right">
										<div class="recaptcha">
											<div style="width: 304px; height: 78px;">
												<iframe title="reCAPTCHA" src="https://www.google.com/recaptcha/api2/anchor?ar=1&amp;k=6LccEIkcAAAAAB2ckEoPDtqFZ_uTF2yjUkhCo7fb&amp;co=aHR0cHM6Ly9ldmVyZ3Jvd3RobWlsay52bjo0NDM.&amp;hl=en&amp;v=g9jXH0OtfQet-V0Aewq23c7K&amp;theme=light&amp;size=normal&amp;cb=jtr28x42wxss" width="304" height="78" role="presentation" name="a-gvqi07f4kk5m" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox allow-storage-access-by-user-activation"></iframe>
												<textarea id="g-recaptcha-response-1" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid rgb(193, 193, 193); margin: 10px 25px; padding: 0px; resize: none; display: none;"></textarea>
											</div>
										</div>
										<div class="form-submit">
											<div class="check" id="check">
												<i class="fas fa-bell bell"></i>
												<i class="fas fa-bell-slash unbell active-check"></i>
											</div>
											<input class="submit-post" type="submit" name="submit" value="Post Comment">
										</div>
									</div>
								</div>
							</div>
							<div class="form-threads">
								<div class="threads-header">
									<div class="threads-info">
										<span class="wptc">0</span>
										Comments
									</div>
									<div class="space"></div>
									<div class="threads-filter">
										<div class="filter filter-left">
											<i class="fas fa-bolt"></i>
										</div>
										<div class="filter filter-right">
											<i class="fas fa-fire"></i>
										</div>
									</div>
								</div>
								<div class="threads-list">
									
								</div>
							</div>
						</div>
					</div>
				</div> -->
				<div class="post-related care-related">
					<h2 class="h2-title">Có thể bạn quan tâm</h2>
					<div class="care-content">
					<?php
					$categories = get_the_category($post->ID);
					if ($categories) {
						$category_ids = array();
						foreach ($categories as $individual_category) $category_ids[] = $individual_category->term_id;
						$args = array(
							'category__in' => $category_ids,
							'post__not_in' => array($post->ID),
							'showposts' => 3,
							'ignore_sticky_posts' => 1
						);
						$my_query = new wp_query($args);
						if ($my_query->have_posts()) { ?>
							<div class="care-list">
								<?php while ($my_query->have_posts()) {
									$my_query->the_post(); ?>
										<?php get_template_part('template-parts/content', 'post-related'); ?>
								<?php } ?>
							</div>
					<?php }
					}

					?>
					</div>
	</div>
</main><!-- #main -->
<?php
get_footer();
