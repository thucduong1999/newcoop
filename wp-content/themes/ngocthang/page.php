<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ngocthang
 */
get_header();
?>
<!-- <nav class="breadcrumbs">
	<div class="container">
		<div class="breadcrumbs-text">
			<?php  
			if(function_exists('yoast_breadcrumb')) {yoast_breadcrumb();}
			?>
			
		</div>
	</div>
</nav> -->
<main class="page__content">
<?php if(is_page('ve-chung-toi')){ ?> 
            <div class="page-ve-chung-toi">
                <section class="header">
                    <div class="header-box about-us">
                        <div class="header-title">
                            <h1></h1>
                        </div>
                        <!-- <div class="header-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/5-copy.png"/>
                        </div> -->
                    </div>
                </section>
                <section class="us">
                    <div class="box">
                        <div class="us-text" data-aos="fade-down" data-aos-duration="2000">
                            <h3>Về chúng tôi</h3>
                        </div>
                        <div class="wrap">
                            <div class="us-container">
                                <div class="us-wrap-box height-1">
                                    <div class="us-wrap-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 168.4 168.4"><title>Asset 19</title><g id="Layer_2" data-name="Layer 2"><g id="Oj"><path d="M84.2,0a84.2,84.2,0,1,0,84.2,84.2A84.2,84.2,0,0,0,84.2,0Z" style="fill:#26bbe3"></path><circle cx="84.2" cy="84.2" r="72.74" style="fill:#fff"></circle><text transform="translate(41.9 98.17)" style="font-size:72.09306335449219px;fill:#26bbe3;font-family:Quicksand-Bold, Quicksand;font-weight:700">20<tspan style="font-size:33.025508880615234px"><tspan x="3.8" y="33">Năm</tspan></tspan></text></g></g></svg>
                                    </div>
                                </div>
                                <div class="us-text-container">
                                    <div class="us-text-item item-color-1">
                                        Chúng tôi – những xã viên Hợp tác xã (HTX) – tự hào cùng nhau đóng góp và xây dựng nhà máy sữa GoAMILK trong suốt gần 20 năm qua.
                                    </div>
                                </div>
                            </div>
                            <div class="us-container">
                                <div class="us-wrap-box height-2">
                                    <div class="us-wrap-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 168.4 168.4"><title>Asset 18</title><g id="Layer_2" data-name="Layer 2"><g id="Oj"><path d="M84.2,0a84.2,84.2,0,1,0,84.2,84.2A84.2,84.2,0,0,0,84.2,0Z" style="fill:#006484"></path><circle cx="84.2" cy="84.2" r="72.74" style="fill:#fff"></circle><text transform="translate(38.69 95.83)" style="font-size:61.807411193847656px;fill:#006484;font-family:Quicksand-Bold, Quicksand;font-weight:700">2<tspan x="33.13" y="0" style="font-size:28.313690185546875px">.000</tspan><tspan style="font-size:28.313690185546875px"><tspan x="29.18" y="31">hộ</tspan></tspan></text></g></g></svg>
                                    </div>
                                </div>
                                <div class="us-text-container">
                                    <div class="us-text-item item-color-2">
                                        Chặng đường của chúng tôi khởi đầu với 171 hộ. Vượt qua nhiều khó khăn, nhờ sự hỗ trợ của Tổ chức Socodevi từ Canada, quy mô HTX đạt 2.000 hộ với gần 10.000 nhân khẩu, trở thành HTX bò sữa số 1 Việt Nam.
                                    </div>
                                </div>
                            </div>
                            <div class="us-container">
                                <div class="us-wrap-box height-1">
                                    <div class="us-wrap-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 168.4 168.4"><title>Asset 17</title><g id="Layer_2" data-name="Layer 2"><g id="Oj"><path d="M84.2,0a84.2,84.2,0,1,0,84.2,84.2A84.2,84.2,0,0,0,84.2,0Z" style="fill:#89ce00"></path><circle cx="84.2" cy="84.2" r="72.74" style="fill:#fff"></circle><text transform="translate(25.57 96.85)" style="font-size:61.807411193847656px;fill:#89ce00;font-family:Quicksand-Bold, Quicksand;font-weight:700">16<tspan x="56.37" y="0" style="font-size:28.313690185546875px">.000</tspan><tspan style="font-size:18.003889083862305px"><tspan x="4.88" y="24.86">lít sữa/n</tspan><tspan x="77.81" y="24.86" style="letter-spacing:-0.004990241251848802em">g</tspan><tspan x="89.12" y="24.86" style="letter-spacing:-0.011987427354984622em">à</tspan><tspan x="100.12" y="24.86">y</tspan></tspan></text></g></g></svg>
                                    </div>
                                </div>
                                <div class="us-text-container">
                                    <div class="us-text-item item-color-3">
                                        Nông trại của chúng tôi có hơn 7.000 con bò, được phát triển từ công nghệ bò giống hiện đại của Canada. Qua từng giai đoạn, tiếp thu quy trình chăn nuôi theo tiêu chuẩn nghiêm ngặt của châu Âu, đến nay, những chú bò của chúng tôi cho sản lượng tới hơn 16.000 lít sữa mỗi ngày.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                
                <section class="information">
                    <div class="information-box">
                        <div class="box">
                            <div class="wrap">
                                <p>Được hình thành từ các chương trình xoá đói giảm nghèo, phát triển bền vững của tỉnh Sóc Trăng, hợp tác xã New Coop đóng vai trò cầu nối giữa người nông dân tới với người tiêu dùng thông qua những sản phẩm nông nghiệp nổi trội của vùng đất trù phú miền Tây Việt Nam.</p>
                                <p>Đó là những dòng sữa tươi nguyên chất từ những chú bò sữa Sóc Trăng, giàu đạm sữa, dồi dào dinh dưỡng, thơm mát, và đặc biệt tươi ngon thuần khiết, phù hợp sử dụng cho mọi thành viên trong mỗi gia đình. Không chỉ vậy, sữa tươi GoA còn cung cấp nhiều axit amin nguồn gốc động vật rất có lợi cho tiêu hoá và chăm sóc sắc đẹp chị em phụ nữ.</p>
                                <p>Đó là gạo ngon nhất thế giới ST25 của vùng đất màu mỡ Sóc Trăng. Sau nhiều năm đưa đặc sản “ngọc trời” này đến với người dân ở các quốc gia châu Âu, tới nay, chúng tôi mong muốn sẽ gửi sản phẩm chất lượng cao này tới người tiêu dùng trong nước.</p>
                                <p>Đó là sự đồng hành hỗ trợ người nông dân Sóc Trăng trong quá trình gây dựng “cơ ngơi nông nghiệp” của riêng mình thông qua các dịch vụ hỗ trợ chăm sóc thú y, tư vấn chăn nuôi và cây/con giống.</p>
                                <p>Hợp tác xã New Coop xem việc đồng hành và đảm bảo sinh kế cho mỗi người nông dân Sóc Trăng trên chặng đường xoá đói giảm nghèo là sứ mệnh của mình. Từ đó, người nông dân Sóc Trăng phát triển cũng là minh chứng cho sự phát triển của Hợp tác xã New Coop, cùng đưa các sản phẩm nông nghiệp tốt tới với tất cả người dân Việt Nam.</p>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="core-value">
                    <div class="core-value-box">
                        <div class="box">
                            <div class="wrap">
                                <h4 data-aos="fade-down" data-aos-duration="2000">Giá trị cốt lõi</h4>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/img-gtcl.png" alt="">
                            </div>
                        </div>
                    </div>
                </section>

                <section class="farm">
                    <div class="box">
                        <div class="farm-box">
                            <div class="wrap">
                                <h4 data-aos="fade-down" data-aos-duration="2000">Nông trại sữa bò của chúng tôi</h4>
                            </div>
                            <div class="farm-list">
                                <div class="wrap farm-wrap">
                                    <div class="farm-item">
                                        <span>
                                            2.000
                                        </span>
                                        <p>Hộ xã viên</p>
                                    </div>
                                </div>
                                <div class="wrap farm-wrap">
                                    <div class="farm-item">
                                        <span>
                                            10.000
                                        </span>
                                        <p>Nhập khẩu</p>
                                    </div>
                                </div>
                                <div class="wrap farm-wrap">
                                    <div class="farm-item">
                                        <span>
                                            7.000
                                        </span>
                                        <p>Chú bò sữa</p>
                                    </div>
                                </div>
                                <div class="wrap farm-wrap">
                                    <div class="farm-item">
                                        <span>
                                            16.000
                                        </span>
                                        <p>Lít sữa tươi/ngày</p>
                                    </div>
                                </div>
                            </div>
                            <p>2.000 hộ xã viên, 10.000 nhân khẩu.<br>Hơn 7.000 chú bò sữa, giống bò Holstien Friesian (HF) có nguồn gốc từ Hà Lan.<br>Trên 16.000 lít sữa tươi mỗi ngày. Sữa New Coop là sữa tươi nguyên chất 100%<br><strong>Sử dụng công nghệ chăn nuôi bò sữa của Canada, được chuyển giao từ Socodevi.</strong><br><strong>Được sự hỗ trợ phát triển từ CCPA – HTX lớn nhất tại Pháp</strong></p>
                        </div>
                    </div>
                </section>

                <section class="hitory">
                    <div class="box">
                        <div class="hitory-box">
                            <div class="wrap">
                                <h4 data-aos="fade-down" data-aos-duration="2000">Lịch sử của chúng tôi</h4>
                            </div>
                            
                            <div class="hitory-list">
                                <div class="hitory-empty-block"></div>
                                <div class="hitory-item">
                                    <div class="hitory-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/img-2003.png" alt="">
                                    </div>
                                    <div class="hitory-text">
                                        <p>Được thành lập ngày 29/3/2003 với sự hỗ trợ từ <b>Tổ chức Socodevi Canada</b>. Đầu tiên có 171 hộ xã viên tham gia, sau đó tăng lên gần 500 hộ, 1.000 hộ và đạt <b>2.000 hộ vào đầu năm 2021.</b></p>
                                    </div>
                                </div>
                            </div>
                            <div class="hitory-list list-margin">
                                <div class="hitory-item">
                                    <div class="hitory-img-2">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/img-2004.png" alt="">
                                    </div>
                                    <div class="hitory-text text-end">
                                        <p>HTX Nông nghiệp GoAMILK là một trong những mô hình <b>tiêu biểu trong lĩnh vực kinh tế tập thể của cả nước</b>, là mô hình mẫu được giới thiệu vượt ra ngoài biên giới. Thành lập nhà máy sản xuất cám bò ngày 21/12/2016</p>
                                    </div>
                                </div>
                            </div>
                            <div class="hitory-list">
                                <div class="hitory-empty-block"></div>
                                <div class="hitory-item">
                                    <div class="hitory-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/img-2019.png" alt="">
                                    </div>
                                    <div class="hitory-text">
                                        <p>Ngày 29/06/2019, <strong>Công ty CP Sữa GoAMILK</strong> từ HTX sữa GoAMILK chính thức khánh thành nhà máy sữa với tổng vốn đầu tư <strong>12 triệu USD</strong> và cho ra đời dòng sản phẩm<strong> sữa tươi nguyên chất</strong> NewCoop. Công suất chế biến 80 tấn sữa tươi/ngày.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="ever-milk">
                    <div class="ever-milk-box">
                        <div class="box">
                            <div class="wrap">
                                <div class="ever-milk-list">
                                    <div class="milk-img-left">
                                        <div class="milk-img-box-left">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/img-nm.png" alt="">
                                        </div>
                                    </div>
                                    <div class="milk-img-right">
                                        <div class="milk-img-box-right">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/img-bo.png" alt="">
                                        </div>
                                    </div>
                                </div>
                                <h4 data-aos="fade-down" data-aos-duration="2000">NewCoop</h4>
                            </div>
                        </div>
                    </div>
                    <div class="ever-milk-box box-background">
                        <div class="box">
                            <div class="wrap">
                                <h4 data-aos="fade-down" data-aos-duration="2000">Sữa Tươi Thuần Khiết Của Sóc Trăng</h4>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="htx">
                    <div class="box">
                        <div class="wrap">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/VHL_8362.jpg" alt="">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/VHL_8437.jpg" alt="">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/VHL_8451.jpg" alt="">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/VHL_8538.jpg" alt="">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/VHL_8555.jpg" alt="">
                        </div>
                    </div>
                </section>
			</div>
		<?php } 
		else { ?> 
		<?php if ( have_posts() ) : ?>
			<?php the_content(); 
		endif; ?>
		<?php }
		?>


		<?php if(is_page('lien-he')){ ?> 
			<div class="page-lien-he">
                <section class="header">
                    <div class="header-box contact">
                        <div class="header-title">
                            <h1>Liên hệ</h1>
                        </div>
                    </div>
                </section>

                <section class="address">
                    <div class="box">
                        <div class="contact-inner row">
                            <div class="contact-inner-right contact wrap col-sm-12 col-lg-6">
                                <div class="test-addr">
                                    <div class="title">
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="icon" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                <g>
                                                <g>
                                                <path d="M421.936,211.786v238.533h-90.064V347.155c0-9.045-7.331-16.375-16.375-16.375H200.324    c-9.045,0-16.375,7.331-16.375,16.375v103.164H94.431V211.786H61.681v254.908c0,9.045,7.331,16.375,16.375,16.375h122.269    c9.045,0,16.375-7.331,16.375-16.375V363.531h82.422v103.164c0,9.045,7.331,16.375,16.375,16.375h122.815    c9.045,0,16.375-7.331,16.375-16.375V211.786H421.936z"></path>
                                                </g>
                                                </g>
                                                <g>
                                                <g>
                                                <path d="M506.815,255.508L269.373,33.351c-6.25-5.857-15.966-5.895-22.27-0.104L5.295,255.405    c-6.659,6.119-7.096,16.474-0.977,23.133c3.226,3.521,7.636,5.3,12.063,5.3c3.957,0,7.931-1.43,11.075-4.318L258.085,67.635    l226.355,211.787c6.616,6.184,16.965,5.83,23.144-0.77C513.758,272.047,513.419,261.687,506.815,255.508z"></path>
                                                </g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                            </svg>
                                        </span>
                                        <span><?php echo of_get_option('cong-ty'); ?></span>
                                    </div>
                                    <div class="desc">
                                        <div class="desc-item">
                                            <span>
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="icon" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                    <g>
                                                    <g>
                                                    <path d="M256,0C150.112,0,64,86.112,64,192c0,133.088,173.312,307.936,180.672,315.328C247.808,510.432,251.904,512,256,512    s8.192-1.568,11.328-4.672C274.688,499.936,448,325.088,448,192C448,86.112,361.888,0,256,0z M256,472.864    C217.792,431.968,96,293.664,96,192c0-88.224,71.776-160,160-160s160,71.776,160,160C416,293.568,294.208,431.968,256,472.864z"></path>
                                                    </g>
                                                    </g>
                                                    <g>
                                                    <g>
                                                    <path d="M256,96c-52.928,0-96,43.072-96,96s43.072,96,96,96c52.928,0,96-43.072,96-96C352,139.072,308.928,96,256,96z M256,256    c-35.296,0-64-28.704-64-64s28.704-64,64-64s64,28.704,64,64S291.296,256,256,256z"></path>
                                                    </g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                </svg>
                                            </span>
                                            <span><?php echo of_get_option('dia-chi'); ?></span>
                                        </div>
                                        <div class="desc-item">
                                            <span>
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="icon" id="Capa_1" x="0px" y="0px" viewBox="0 0 482.6 482.6" style="enable-background:new 0 0 482.6 482.6;" xml:space="preserve">
                                                    <g>
                                                    <path d="M98.339,320.8c47.6,56.9,104.9,101.7,170.3,133.4c24.9,11.8,58.2,25.8,95.3,28.2c2.3,0.1,4.5,0.2,6.8,0.2   c24.9,0,44.9-8.6,61.2-26.3c0.1-0.1,0.3-0.3,0.4-0.5c5.8-7,12.4-13.3,19.3-20c4.7-4.5,9.5-9.2,14.1-14   c21.3-22.2,21.3-50.4-0.2-71.9l-60.1-60.1c-10.2-10.6-22.4-16.2-35.2-16.2c-12.8,0-25.1,5.6-35.6,16.1l-35.8,35.8   c-3.3-1.9-6.7-3.6-9.9-5.2c-4-2-7.7-3.9-11-6c-32.6-20.7-62.2-47.7-90.5-82.4c-14.3-18.1-23.9-33.3-30.6-48.8   c9.4-8.5,18.2-17.4,26.7-26.1c3-3.1,6.1-6.2,9.2-9.3c10.8-10.8,16.6-23.3,16.6-36s-5.7-25.2-16.6-36l-29.8-29.8   c-3.5-3.5-6.8-6.9-10.2-10.4c-6.6-6.8-13.5-13.8-20.3-20.1c-10.3-10.1-22.4-15.4-35.2-15.4c-12.7,0-24.9,5.3-35.6,15.5l-37.4,37.4   c-13.6,13.6-21.3,30.1-22.9,49.2c-1.9,23.9,2.5,49.3,13.9,80C32.739,229.6,59.139,273.7,98.339,320.8z M25.739,104.2   c1.2-13.3,6.3-24.4,15.9-34l37.2-37.2c5.8-5.6,12.2-8.5,18.4-8.5c6.1,0,12.3,2.9,18,8.7c6.7,6.2,13,12.7,19.8,19.6   c3.4,3.5,6.9,7,10.4,10.6l29.8,29.8c6.2,6.2,9.4,12.5,9.4,18.7s-3.2,12.5-9.4,18.7c-3.1,3.1-6.2,6.3-9.3,9.4   c-9.3,9.4-18,18.3-27.6,26.8c-0.2,0.2-0.3,0.3-0.5,0.5c-8.3,8.3-7,16.2-5,22.2c0.1,0.3,0.2,0.5,0.3,0.8   c7.7,18.5,18.4,36.1,35.1,57.1c30,37,61.6,65.7,96.4,87.8c4.3,2.8,8.9,5,13.2,7.2c4,2,7.7,3.9,11,6c0.4,0.2,0.7,0.4,1.1,0.6   c3.3,1.7,6.5,2.5,9.7,2.5c8,0,13.2-5.1,14.9-6.8l37.4-37.4c5.8-5.8,12.1-8.9,18.3-8.9c7.6,0,13.8,4.7,17.7,8.9l60.3,60.2   c12,12,11.9,25-0.3,37.7c-4.2,4.5-8.6,8.8-13.3,13.3c-7,6.8-14.3,13.8-20.9,21.7c-11.5,12.4-25.2,18.2-42.9,18.2   c-1.7,0-3.5-0.1-5.2-0.2c-32.8-2.1-63.3-14.9-86.2-25.8c-62.2-30.1-116.8-72.8-162.1-127c-37.3-44.9-62.4-86.7-79-131.5   C28.039,146.4,24.139,124.3,25.739,104.2z"></path>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                    <g>
                                                    </g>
                                                </svg>
                                            </span>
                                            <span><?php echo of_get_option('hotline'); ?></span>
                                        </div>
                                        <div class="desc-item">
                                            <span>
                                                <svg xmlns="http://www.w3.org/2000/svg" class="icon" id="Capa_1" viewBox="0 0 479.058 479.058"><path d="m434.146 59.882h-389.234c-24.766 0-44.912 20.146-44.912 44.912v269.47c0 24.766 20.146 44.912 44.912 44.912h389.234c24.766 0 44.912-20.146 44.912-44.912v-269.47c0-24.766-20.146-44.912-44.912-44.912zm0 29.941c2.034 0 3.969.422 5.738 1.159l-200.355 173.649-200.356-173.649c1.769-.736 3.704-1.159 5.738-1.159zm0 299.411h-389.234c-8.26 0-14.971-6.71-14.971-14.971v-251.648l199.778 173.141c2.822 2.441 6.316 3.655 9.81 3.655s6.988-1.213 9.81-3.655l199.778-173.141v251.649c-.001 8.26-6.711 14.97-14.971 14.97z"></path>
                                                </svg>
                                            </span>
                                            <span><?php echo of_get_option('email'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="map wrap col-sm-12 col-lg-6">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.674928002218!2d105.85264231476285!3d21.00566388601089!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac0a8d3eabc5%3A0xbc5d6e1f03c8f80f!2zODAgVsO1IFRo4buLIFPDoXUsIFRoYW5oIE5ow6BuLCBIYWkgQsOgIFRyxrBuZywgSMOgIE7hu5lpLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1653290672325!5m2!1svi!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="form-contact">
                    <div class="box">
                        <div class="form-inner row">
                            <div class="background-img col-sm-12 col-lg-6" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/background-img-vct.jpg)">
                                <div class="form-img wrap">
                                <img src="<?php echo of_get_option('lien-he_image'); ?>" />
                                </div>
                            </div>

                            <div class="form-content wrap col-sm-12 col-lg-6">
                                <div class="title mb-20">Liên hệ</div>
                                <div class="desc mb-20">Đừng ngần ngại, chỉ cần cho chúng tôi biết về bản thân bạn và chúng tôi sẽ tìm ra lựa chọn tốt nhất cho bạn hoặc công ty của bạn</div>
                                <?php echo do_shortcode('[contact-form-7 id="492" title="Form liên hệ"]'); ?>
                            </div>
                        </div>
                    </div>
                </section>
			</div>
		<?php } 
		else { ?> 
		<?php if ( have_posts() ) : ?>
			<?php the_content(); 
		endif; ?>
		<?php }
		?>


</main><!-- #main -->
<?php
get_footer();