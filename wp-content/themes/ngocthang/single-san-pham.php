<?php

/**

 * The template for displaying all single posts

 *

 * @package WordPress

 * @subpackage KENIT_GYM

 * @since KENIT GYM 1.0

 * @version 1.0

 */



get_header(); ?>

<div class="breadcrumbs">
    <div class="header header-san-pham">
        <div class="header-title">
            <h1><?php the_title(); ?></h1>
        </div>
    </div>
</div>
<section class="details details-product">
  <div class="container">
        <?php
        /* Start the Loop */
        while ( have_posts() ) :
          the_post(); ?>
              <div class="box details-inner">
                  <div class="box-item-product wrap">
                      <div class="box-item-inner">
                      <?php the_post_thumbnail('full', array( 'class' => 'center-block' ) ); ?>    
                      </div>
                  </div>
                  <div class="box-item-product wrap wid-33">
                      <div class="box-item-inner border-nutri">
                          <?php echo  $value = rwmb_meta( 'info-pro' );?>
                      </div>
                  </div>
                  <div class="box-item-product wrap">
                      <div class="box-item-inner">
                        <?php the_content(); ?>
                      </div>
                  </div>
                  <div class="box-item-product wrap jcl wid-33">
                      <div class="box-item-inner">
                        <?php echo  $value = rwmb_meta( 'info-pro-2' );?>
                      </div>
                  </div>
              </div>          
        <?php endwhile; // End of the loop.
        ?>
    </div>
  <div class="other-product post-related container">
    <h2 class="main-loop-heading">Sản phẩm khác</h2>
        <div class="list-product">
          <?php  
          
          $terms_obj = wp_get_post_terms( get_the_id(), 'cat-san-pham' );
          $terms     = array();
          foreach ($terms_obj as $term_ob) {
            $terms[]   = $term_ob->term_id;
          }

          $my_query = new WP_Query( array(
            'post_type'           => 'san-pham',
            'posts_per_page'      => 3,
            'ignore_sticky_posts' => 1,
            'post__not_in'        => array( get_the_ID() ),
            'tax_query' =>   array(
              array(
                'taxonomy'         => 'cat-san-pham',
                'field'            => 'id',
                'terms'            => $terms,
                'include_children' => true,
              ),
            ),
          ) );

          if( $my_query->have_posts() ) { ?>
            <?php while ($my_query->have_posts()) { $my_query->the_post(); ?>
              <div class="row-item">
                  <div class="row-box-img">
                      <a href="<?php the_permalink(); ?>">
                          <div class="row-item-img">
                          <?php the_post_thumbnail('full', array( 'class' => 'img-fluid center-block' ) ); ?>
                            <div class="row-background"></div>
                              <div class="product-row-details">
                                  <div class="details">
                                      Chi tiết
                                  </div>
                              </div>
                          </div>
                      </a>
                  </div>
                  <div class="row-item-text">
                      <a href="<?php the_permalink(); ?>">
                          <span><?php the_title(); ?></span>
                      </a>
                  </div>
              </div>
            <?php }?>
          <?php } 


          ?>
          </div>
        </div>
</section>

<?php get_footer();

