<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ngocthang
 */

get_header();
?>
<nav class="breadcrumbs news-background">
	<div class="container">
	<div class="header-title">
		<?php
		the_archive_title( '<h1 class="h1-title">', '</h1>' );
		the_archive_description( '<div class="archive-description">', '</div>' );
		?>
		</div>
	</div>
</nav>
<main class="archive__content">
	<div class="container">
		<div class="news-box">
			<div class="news-content-list">
				<div class="section-header widget-title">Mới nhất</div>
				<div class="content-news">
				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?> 
						<?php get_template_part( 'template-parts/content', 'post'); ?>
						<?php endwhile;

						the_posts_navigation(); 
						else :

						get_template_part( 'template-parts/content', 'none' );

						endif;
						?>
					</div>	
			</div>
			<div class="promotion-outstanding">
				<div class="section-header widget-title">Nổi bật</div>
				<?php
				$args = array(
					'post_type' => 'post',
					'category_name' => 'tin-tuc',
					'orderby' => 'date',
					'order' => 'DESC',
					'posts_per_page' => 4,
				);
				$the_query = new WP_Query( $args );
				// The Loop
				if ( $the_query->have_posts() ) :
					while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<?php get_template_part('template-parts/content','post-sidebar'); ?>
					<?php endwhile;
				endif;
				// Reset Post Data
				wp_reset_postdata();
				?>
			</div>
		</div>
	</div>
</main><!-- #main -->
	
<?php

get_footer();
