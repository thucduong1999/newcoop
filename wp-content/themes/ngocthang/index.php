<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>

<main id="site-content" role="main">
	<div class="banner">
		<?php  $args = array(
				'post_type' => 'slides', 
				'showposts' => 1,
				'order' => 'DESC', 
				'orderby' => 'ID',                          
			); 
			$news = new WP_Query($args); ?>
			<?php  if ($news->have_posts()) : ?>
				<?php while ($news->have_posts()) : $news->the_post();?>
						<?php the_post_thumbnail('full', array( 'class' => 'img-fluid center-block' ) ); ?>
				<?php endwhile; ?>
				<?php else : ?><!-- Slides -->
				<div class="alert alert-danger notice text-center" role="alert"><?php _e('Rất tiếc, mục này chưa có dữ liệu.','kenit'); ?></div>
			<?php endif; wp_reset_query();?> 
	</div>

	<div class="milk-img-box">
		<div class="categoty-milk">
			<?php for($i=1;$i<=6;$i++) { ?>
					<div class="milk-box-list">
						<div class="wrap-5">
							<a href="<?php echo of_get_option('link-cate'.$i); ?>">
								<div class="milk-image-box">
									<div class="milk-img-list">
										<div class="milk-img-item">
											<img src="<?php echo of_get_option('image-cate'.$i); ?>">
										</div>
										<div class="milk-img-text"><?php echo of_get_option('name-cate'.$i); ?></div>
									</div>
								</div>
							</a>
						</div>
					</div>
				<?php }
				?>
		</div>
	</div>

	<div class="title-pick">
		<h2 class="elementor-heading-title elementor-size-default">GoA MILK - Sữa tươi dành cho gia đình bạn</h2>
	</div>

	<div class="wrapper-info-milk">
		<div class="info-milk">
			<div class="content-milk content-milk-mobile">
			<?php for($i=1;$i<=5;$i++) { ?>
				<div class="name">
					<div class="lable-mikle lable-mikle-left">
						<?php echo of_get_option('loi-ich-left'.$i); ?>
					</div>
					<div class="icon-success">
						<img src="http://newcoop.acstech.vn/wp-content/themes/ngocthang/assets/images/icon.JPG" />
					</div>
				</div>
				<?php }
				?>
			</div>
			<div class="content-milk background-content-milk">
				<img src="<?php echo of_get_option('loi-ich_image'); ?>">
			</div>
			<div class="content-milk">
				<?php for($i=1;$i<=5;$i++) { ?>
					<div class="name name-mobile">
						<div class="icon-success">
							<img src="http://newcoop.acstech.vn/wp-content/themes/ngocthang/assets/images/icon.JPG" />

						</div>
						<div class="lable-mikle lable-mikle-right">
							<?php echo of_get_option('loi-ich-right'.$i); ?>
						</div>

					</div>
					<?php }
					?>
			</div>
		</div>
	</div>

	<div class="wrapper-cooperate-index">
		<div class="wrapper-cooperate">
			<div class="container-cooperate">
				<span></span>
				<div class="elementor-divider__text elementor-divider__element"><?php echo of_get_option('sub-tittle'); ?></div>
				<span></span>
			</div>

			<div class="elementor-widget-container">
				<h2 class="elementor-heading-title elementor-size-default"><?php echo of_get_option('title'); ?></h2>
			</div>
			<div class="elementor-widget-container conent-elementor-widget-container">
				<?php echo of_get_option('content'); ?>
			</div>
			<div class="elementor-widget-container conent-elementor-widget-container">Vui lòng điền thông tin của bạn vào form dưới đây.</div>
			<?php echo do_shortcode('[contact-form-7 id="492" title="Đăng ký liên hệ"]'); ?>
			<div class="elementor-widget-container">
				<img src="<?php echo of_get_option('home_pr_image'); ?>" />
			</div>
		</div>
	</div>

	<div class="wrapper-blog">
		<div class="none-blog">
			<div class="elementor-widget-container">
				<h2 class="elementor-heading-title elementor-size-default">Blog</h2>
			</div>
			<div class="elementor-widget-container">
				<h5 class="elementor-heading-title elementor-size-default">Công thức chế biến đồ ăn<br>Thức uống cùng sữa</h5>
			</div>

			<div id="carouselExampleSlidesOnly" class="carousel slide carousel-mobile" data-ride="carousel">
				<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="carousel-item active">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/z3324826461273_fc76ac7479e8d9a6034b9b376bf93b2f.jpg" />
							<div class="title-blog">
								<h3>
									GoAMILK – SỮA TƯƠI CỦA NGƯỜI VIỆT NAM
								</h3>
								<button class="see-more-button" type="submit">
									<a href="#">Xem thêm</a>
								</button>

							</div>
						</div>

						<div class="carousel-item">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/z3324826461273_fc76ac7479e8d9a6034b9b376bf93b2f.jpg" />
							<div class="title-blog">
								<h3>
									Người mắc Covid-19 nên ăn những thực phẩm nào?
								</h3>
								<button class="see-more-button" type="submit">
									<a href="#">Xem thêm</a>
								</button>
							</div>
						</div>
					</div>
					<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>

			<div class="item-blog">
				<div class="content-blog">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/z3324826461273_fc76ac7479e8d9a6034b9b376bf93b2f.jpg">
					<div class="title-blog">
						<h3>
							GoAMILK – SỮA TƯƠI CỦA NGƯỜI VIỆT NAM
						</h3>
						<button class="see-more-button" type="submit">Xem thêm</button>

					</div>

				</div>
				<div class="content-blog">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/z3324826461273_fc76ac7479e8d9a6034b9b376bf93b2f.jpg">
					<div class="title-blog">
						<h3>
							Người mắc Covid-19 nên ăn những thực phẩm nào?
						</h3>
						<button class="see-more-button" type="submit">Xem thêm</button>

					</div>
				</div>
			</div>
		</div>
		<div class="wrapper-new-products">
			<div class="elementor-widget-container lable-new-products">
				<h2 class="elementor-heading-title elementor-size-default">Sản phẩm mới</h2>
			</div>
			<div class="item-new-products">
				<?php
					$project_post_args = array(                 
						'post_type' => 'san-pham',
						'posts_per_page' => '3',
					); ?>
					<?php $wp_query = new WP_Query( $project_post_args );
					if ( $wp_query -> have_posts() ) : 
						while ( $wp_query -> have_posts() ) : $wp_query -> the_post(); ?>
								<div class="content-new-products">
									<div class="row-box-img">
										<a href="<?php the_permalink(); ?>">
											<div class="row-item-img">
												<?php the_post_thumbnail('full', array( 'class' => 'img-fluid center-block' ) ); ?>
												<div class="row-background" style="background-image: url(./image/2022.317.png)"></div>
												<div class="product-row-details">
													<div class="details"><a href="<?php the_permalink(); ?>">Chi tiết</a></div>
												</div>
											</div>
										</a>
									</div>
									<div class="wrapper-title-products">
										<a href="<?php the_permalink(); ?>" class="title-new-products"><?php the_title(); ?></a>
									</div>
								</div>
							<?php endwhile; endif; ?>
							<?php wp_reset_postdata(); ?>
			</div>
		</div>


		<section class="blogs tin-moi-nhat">
			<div class="container">
				<div class="heading-section text-center">
					<h2>Tin mới nhất</h2>
				</div>
				<div class="blogs-all">
					<?php
					$project_post_args = array(                 
						'post_type' => 'post',
						'category_name' => 'tin-tuc',
						'order' => 'DESC',
						'orderby' => 'date', 
						'posts_per_page' => '3',
					); ?>
					<?php $wp_query = new WP_Query( $project_post_args );
					if ( $wp_query -> have_posts() ) : 
						while ( $wp_query -> have_posts() ) : $wp_query -> the_post(); ?>
							<div class="blogs-item">
								<div class="blogs-img">
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full', array( 'class' => 'img-fluid center-block' ) ); ?></a>
								</div>
								<div class="blogs-content">
									<div class="date"><span>
										<?php the_time('d/m/Y'); ?></span></div>
										<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
										<p><?php echo ngocthang_limit_words(get_the_excerpt(),30); ?></p>
										<div class="view-now"><a href="<?php the_permalink(); ?>">Đọc tiếp</a></div> 
									</div>
								</div>

							<?php endwhile; endif; ?>
							<?php wp_reset_postdata(); ?>
							
							</div>
						<div class="view-more">
							<a href="">Xem thêm</a>
						</div>
					</div>
				</section>

</main><!-- #site-content -->
<?php
get_footer(); ?>