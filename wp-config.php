<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'newcoop' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '/Y_Zb^e?0AnBC^Q}Lb`QGtFcOVHA>[nEV&k+~XLS?oq<$qMXkLohAfw<7J)+#f[v' );
define( 'SECURE_AUTH_KEY',  '+E%yQH@(IA{,;PhYuRpU%Qo)i2=6Rp{hY3DQhfvuu)c!LMs_7t@tt]QDf&<zF01i' );
define( 'LOGGED_IN_KEY',    '%YEJ`sJ^<Ygr>*A`h<f%!L:XrH#1l<P,Q<Nz[xs1^QEVCicqbYzNc)/Ex<99SpF[' );
define( 'NONCE_KEY',        'jg|[mc^Gk7 OE{3Ukix 1dcV*wFHFL<rDC-JAm;uYf7 Rka)7EkYhjo9.@B4v*+Z' );
define( 'AUTH_SALT',        'n5~*%A)=dUTH16`i<CA/nCeOjvvLnrh{-U<_f;p-4Yf|FfRg#]znOHu|ob66;{VJ' );
define( 'SECURE_AUTH_SALT', '73+]n^sq}972no4A!X%]Wmqqt<v6*;:0)8qs{(EdYu/cS#mb2}|nTww:GoFm:#Ca' );
define( 'LOGGED_IN_SALT',   '&KyM~<I@SOVNadhed>[ K[.YX(?=nv^4Y}z!@m}M&y-@%rTnH;|2U)ZY~BQ$gjGp' );
define( 'NONCE_SALT',       'Z#Z+1S&pe !o4^K`1cxnN?inBtnZ`SGkfy7y~`N/9EP7@Hm&A?n~?T!D4J#)`W!r' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
